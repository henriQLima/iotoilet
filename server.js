const express = require('express'),
    app = express(),
    request = require('request');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Production environment
const PORT = Number(process.env.PORT || 8080)

// Allows CORS
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "HEAD, GET, POST, OPTIONS, PUT, PATCH, DELETE");
    next();
});

if (process.env.NODE_ENV === 'production') {
	app.use(express.static('./build'));
}

let max = 10.2, min = 28.5, diff = min - max, distance, tankValue, percent, tankStatus, flushCounter = 0, lastChecked, flushStamp = 1540358676697;

//run function every 5mins
setInterval(requestData = () => {
    request('http://blynk-cloud.com/03ffac3bf3ce4d4b91ab187870460bca/get/V1', function (error, response, body) {
        if (body.length > 21) {
            newArr = body.slice(0, -1).split(",")
            console.log(newArr)
            distance = newArr[3].slice(1, -1);
            console.log(distance)
            tankValue = parseFloat(distance) - max;
            percent = (((tankValue * 100) / diff) - 100) * (-1);
            tankStatus = percent;
        }

        if (tankStatus < 35 && (flushStamp + 600000 < Date.now())) {
            flushCounter++;
            flushStamp = Date.now();
        }

        lastChecked = new Date();

    });
}, 10000)

//firstRun
requestData()

app.get('/api/toiletdata', (req, res) => {
    let data = {
        lastChecked: flushStamp,
        tankStatus: tankStatus,
        sensorDistance: distance,
        flushCounter: flushCounter
    }
    console.log(data)
    res.json(data)
})

app.listen(PORT, () => { console.log('Server running on ' + PORT + '\nCTRL+C to stop'); });