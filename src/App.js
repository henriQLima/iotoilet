import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import { List } from 'react-mdl/lib/List';
import { ListItem } from 'react-mdl/lib/List';
import { Grid, Cell, Icon, ProgressBar, Header } from 'react-mdl';
import moment from 'moment';


class App extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      loaded: false
    };
  };

  // Redirects back to login screen if not already logged in or if token is not verified
  compTest() {
    axios.get('/api/toiletdata')
      .then(result => {
        console.log(result.data)
        this.setState({ data: result.data, loaded: true })
      });
  };

  interval = setInterval(() => this.compTest(), 3000);

  render() {
    if (this.state.loaded) {
      return (
        <div className="App">
          <Grid className="demo-grid-1">
            <Cell col={4}></Cell>
            <Cell col={4}>
              <h4>IOT Water Sensor HL•</h4>
              <p></p>
              <ProgressBar indeterminate />
              <List>
                <ListItem>READY TO USE?<ListItem col={1}>{this.state.data.tankStatus > 90 ? <Icon name="check_circle_outline" style={{ color: "green" }} /> : <Icon name="warning" color="error" style={{ color: "red" }} />}</ListItem></ListItem>
                <ListItem>Toilet Fill %:<ListItem>{this.state.data.tankStatus ? (this.state.data.tankStatus).toFixed(2) + '%' : 'null'}</ListItem></ListItem>
                <ListItem>Last Flush:<ListItem>{this.state.data.lastChecked ? moment(this.state.data.lastChecked).format("h:mm:ss") : 'null'}</ListItem></ListItem>
                <ListItem>Flush Counter:<ListItem>{this.state.data.flushCounter ? this.state.data.flushCounter : '0'}</ListItem></ListItem>
                <ListItem>Water Usage(L):<ListItem>{this.state.data.flushCounter ? (this.state.data.flushCounter * 12) : '0'}</ListItem></ListItem>
                <ListItem>Sensor Distance:<ListItem>{this.state.data.sensorDistance ? this.state.data.sensorDistance : 'null'}cm</ListItem></ListItem>
              </List>
            </Cell>
            <Cell col={4}></Cell>
          </Grid>
        </div>
      );
    }else{
    return (
      <div className="App">
          <Grid className="demo-grid-1">
            <Cell col={4}></Cell>
            <Cell col={4}>
              <h4>Loading...<img src="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/160/apple/129/toilet_1f6bd.png" height="35"/></h4>
              <p></p>
              <ProgressBar indeterminate />
            </Cell>
            <Cell col={4}></Cell>
          </Grid>
        </div>
      )
    }
  }
}

export default App;